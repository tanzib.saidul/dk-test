/**
 * @file Main entry file and server initialization
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */
require('dotenv').config();
const express = require('express');
const compression = require('compression');
const cors = require('cors');
const morgan = require('morgan');

const app = express();
const helmet = require('helmet');
const config = require('./config')();

const { connectDb } = require('./lib/client/mongo');

connectDb();

app.use(compression());
app.use(helmet());
app.use(cors());
app.options('*', cors());
app.use(morgan('combined'));
app.use(express.json());

const randomData = require('./lib/routes/randomData');

app.use('/', randomData);

app.listen(config.server.port, () => console.log('info', `listening on port ${config.server.port}!`));

module.exports = app;
