require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');

const { expect } = chai;
const mongoose = require('mongoose');
const app = require('../../../index');
const { GET_RECORDS } = require('../../__mocks__');
const { generateRandomData } = require('../../../lib/controllers');
const { RandomData } = require('../../../lib/models/randomData');

chai.use(chaiHttp);

describe('Random Data routes tests', () => {
  after(() => {
    mongoose.connection.close();
  });
  describe('#POST [/random]', () => {
    it('should generate 10000 random data', (done) => {
      chai
        .request(app)
        .post('/random')
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          const result = await RandomData.find({});
          expect(result.length).gte(10000);
          done();
        });
    }).timeout(10000);
  });

  describe('#GET [/random]', () => {
    before(async () => {
      await generateRandomData(10);
    });

    it('should send all the records based on query', (done) => {
      chai
        .request(app)
        .get('/random')
        .query(GET_RECORDS)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.result.total).gte(1);
          done();
        });
    }).timeout(10000);
  });
  describe('#PUT [/random/status-delete]', () => {
    before(async () => {
      await generateRandomData(10);
    });

    it('should update all records with ActiveIndicator [Y] to [N] and ExpiryDate to current date', (done) => {
      chai
        .request(app)
        .put('/random/status-delete')
        .query(GET_RECORDS)
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          const result = await RandomData.find({ ActiveIndicator: 'Y' });
          expect(result.length).equal(0);
          done();
        });
    }).timeout(10000);
  });
  describe('#PUT [/random/status-expired]', () => {
    before(async () => {
      await generateRandomData(10);
    });

    it('should update all records with ActiveIndicator [Y] and ExpiryDate to current date', (done) => {
      chai
        .request(app)
        .put('/random/status-expired')
        .query(GET_RECORDS)
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          const result = await RandomData.where({ ActiveIndicator: 'Y', ExpiryDate: null }).limit(10).exec();
          expect(result.length).equal(0);
          done();
        });
    }).timeout(10000);
  });
  describe('#GET [/random/count/active]', () => {
    before(async () => {
      await generateRandomData(100);
    });

    it('should get the count for active an inactive records', (done) => {
      chai
        .request(app)
        .get('/random/count/active')
        .query(GET_RECORDS)
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.totalActive).gte(0);
          expect(res.body.totalInactive).gte(0);
          done();
        });
    }).timeout(10000);
  });
  describe('#GET [/random/count/month]', () => {
    before(async () => {
      await generateRandomData(100);
    });

    it('should get the count for active records by each month', (done) => {
      chai
        .request(app)
        .get('/random/count/month')
        .query(GET_RECORDS)
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          const months = [
            'JAN',
            'FEB',
            'MAR',
            'APR',
            'MAY',
            'JUN',
            'JUL',
            'AUG',
            'SEP',
            'OCT',
            'NOV',
            'DEC',
          ];
          months.forEach((month) => {
            expect(res.body[month]).gte(0);
          });
          done();
        });
    }).timeout(10000);
  });
  describe('#DELETE [/random]', () => {
    before(async () => {
      await generateRandomData(100);
    });

    it('should remove all records from db', (done) => {
      chai
        .request(app)
        .delete('/random')
        .query(GET_RECORDS)
        .end(async (err, res) => {
          expect(res).to.have.status(200);
          const result = await RandomData.find({});
          expect(result.length).equal(0);
          done();
        });
    }).timeout(10000);
  });
});
