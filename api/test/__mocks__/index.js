const GET_RECORDS = {
  page: 0,
  limit: 10,
  sort: 'FirstName',
  searchKey: 'CompanyName',
  searchValue: 'as',
};

module.exports = {
  GET_RECORDS,
};
