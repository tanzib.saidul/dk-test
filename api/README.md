# [REST API SERVICE]

This is a Node.js/Express project for serving dashboard requests.

## Table of Contents

* [Quick Start](#quick-start)
* [File Structure](#file-structure)

## Quick Start

- You need to run MongoDb server prior running this project

- Assume MongoDb is running without authentication.

- Set below environment variable.

```
MONGO_HOST = [your host]
MONGO_PORT = [your port]
PORT = [app running port]
```
- You will need ```node``` and ```npm``` installed globally on your machine.

- Start app

```
$ npm install
$ npm run start
```

- Test and test coverage

```
$ npm run test
$ npm run coverage
```

## File Structure

Within the api project you'll find the following directories and files:

```
api
.
├── package.json
├── README.md
├── index.js
├── config
│   └── index.js
├── test
│   ├── __mocks__
│   │   └── index.js
│   └── api
│       └── routes
│          └── randomData.spec.js
└── lib
    ├── client
    │   └── mongo.js
    ├── controllers
    │   ├── deleteAllRandomData.js
    │   ├── generateRandomData.js
    │   ├── getActiveCount.js
    │   ├── getMonthlyCount.js
    │   ├── getRandomData.js
    │   ├── index.js
    │   ├── statusDelete.js
    │   └── statusExpired.js
    ├── models
    │   └── randomData.js
    ├── routes
    │   └── randomData.js
    └── sanitizers
        └── randomDataSanitizers.js
   
```
