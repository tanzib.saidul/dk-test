const mongoose = require('mongoose');

const randomDataSchema = new mongoose.Schema(
  {
    FirstName: String,
    LastName: String,
    Age: {
      type: Number, min: 18, max: 65,
    },
    Gender: String,
    JobTitle: String,
    Married: Boolean,
    CompanyName: String,
    Phone: String,
    Email: String,
    Country: String,
    ActiveIndicator: {
      type: String,
      default: 'Y',
    },
    ExpiryDate: {
      type: Date, default: null,
    },
    EffectiveDate: {
      type: Date, default: Date.now,
    },
  },
  { timestamps: true },
);

const RandomData = mongoose.model('Random_Data', randomDataSchema);

module.exports = { RandomData };
