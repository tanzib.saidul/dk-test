/**
 * @file Random data related routes
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */

const express = require('express');

const router = express.Router();
const {
  generateRandomData, deleteAllRandomData, statusDelete, statusExpired,
  getActiveCount, getMonthlyCount, getRandomData,
} = require('../controllers');

router.post('/random', async (req, res) => {
  try {
    await generateRandomData();
    return res.status(200).send();
  } catch (e) {
    return res.status(500).json(e);
  }
});

router.get('/random', async (req, res) => {
  try {
    const data = await getRandomData(req.query);
    return res.status(200).json(data);
  } catch (e) {
    return res.status(e.code || 500).json(e);
  }
});

router.delete('/random', async (req, res) => {
  try {
    await deleteAllRandomData();
    return res.status(200).send();
  } catch (e) {
    return res.status(500).json(e);
  }
});

router.put('/random/status-delete', async (req, res) => {
  try {
    await statusDelete();
    return res.status(200).send();
  } catch (e) {
    return res.status(500).json(e);
  }
});

router.put('/random/status-expired', async (req, res) => {
  try {
    await statusExpired();
    return res.status(200).send();
  } catch (e) {
    return res.status(500).json(e);
  }
});

router.get('/random/count/active', async (req, res) => {
  try {
    const data = await getActiveCount();
    return res.status(200).json(data);
  } catch (e) {
    return res.status(500).json(e);
  }
});

router.get('/random/count/month', async (req, res) => {
  try {
    const data = await getMonthlyCount();
    return res.status(200).json(data);
  } catch (e) {
    return res.status(500).json(e);
  }
});

module.exports = router;
