/**
 * @file Remove all random Data
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */

const { RandomData } = require('../models/randomData');

const deleteAllRandomData = () => RandomData.remove({});

module.exports = {
  deleteAllRandomData,
};
