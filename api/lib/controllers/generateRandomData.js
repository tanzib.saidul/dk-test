/**
 * @file Generate 10,000 Random data
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */
const faker = require('faker');
const { RandomData } = require('../models/randomData');

const generateRandomData = (size = 10000) => {
  const randomData = [];
  for (let i = 0; i < size; i += 1) {
    const randomNumber = faker.random.number({
      min: 0,
      max: 99999,
    });

    randomData.push({
      FirstName: faker.name.firstName(),
      LastName: faker.name.lastName(),
      Age: faker.random.number({
        min: 18,
        max: 65,
      }),
      Gender: randomNumber % 2 === 0 ? 'Male' : 'Female',
      JobTitle: faker.name.jobTitle(),
      Married: randomNumber % 2 === 0,
      CompanyName: faker.company.companyName(),
      Phone: faker.phone.phoneNumber(),
      Email: faker.internet.email(),
      Country: faker.address.country(),
      ActiveIndicator: randomNumber % 3 === 0 && randomNumber % 5 === 0 ? 'N' : 'Y',
      EffectiveDate: faker.date.between('2019-01-01', '2019-12-31'),
    });
  }
  return RandomData.insertMany(randomData);
};

module.exports = {
  generateRandomData,
};
