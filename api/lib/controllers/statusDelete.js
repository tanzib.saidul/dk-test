/**
 * @file Update records as delete
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */

const { RandomData } = require('../models/randomData');

const statusDelete = () => RandomData.update({ ActiveIndicator: 'Y' }, { ActiveIndicator: 'N', ExpiryDate: Date.now() }, { multi: true });

module.exports = {
  statusDelete,
};
