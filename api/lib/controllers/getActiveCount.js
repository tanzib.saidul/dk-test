/**
 * @file Get Active records count
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */

const { RandomData } = require('../models/randomData');

const getActiveCount = async () => {
  try {
    const countActive = await RandomData.where({ ActiveIndicator: 'Y' }).countDocuments();
    const countInactive = await RandomData.where({ ActiveIndicator: 'N' }).countDocuments();
    return Promise.resolve({
      totalActive: countActive,
      totalInactive: countInactive,
    });
  } catch (e) {
    return Promise.reject(e);
  }
};

module.exports = {
  getActiveCount,
};
