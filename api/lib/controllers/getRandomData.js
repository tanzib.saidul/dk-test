/**
 * @file Search random data records with query
 *
 * @author Saidul Islam Bhuiyan
 * @since 19 July, 2020
 */

const { RandomData } = require('../models/randomData');
const { getRandomDataRequest } = require('../sanitizers/randomDataSanitizers');

const getRandomData = async (query) => {
  const result = getRandomDataRequest.validate(query, { abortEarly: true });
  if (result.error) {
    return Promise.reject({ code: 422, error: result.error.details[0].message });
  }
  let searchQuery = RandomData;
  let countTotalQuery = RandomData;
  if (result.value.searchKey && result.value.searchValue) {
    countTotalQuery = countTotalQuery.count({ [result.value.searchKey]: { $regex: result.value.searchValue, $options: 'i' } });
    searchQuery = searchQuery.find({ [result.value.searchKey]: { $regex: result.value.searchValue, $options: 'i' } });
  } else {
    searchQuery = searchQuery.find({});
    countTotalQuery = countTotalQuery.count({});
  }
  searchQuery = searchQuery
    .find({})
    .sort(result.value.sort)
    .limit(result.value.limit)
    .skip(result.value.limit * result.value.page);

  try {
    const countTotalDocument = await countTotalQuery.exec();
    const queryResult = await searchQuery.exec();
    const responseData = {
      request: query,
      total_matched: countTotalDocument,
      result: {
        total: queryResult.length,
        data: queryResult,
      },
    };
    return Promise.resolve(responseData);
  } catch (e) {
    return Promise.reject(e);
  }
};

module.exports = {
  getRandomData,
};
