/**
 * @file Get Records count by month
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */

const { RandomData } = require('../models/randomData');

const getMonthlyCount = async () => {
  const months = [
    'JAN',
    'FEB',
    'MAR',
    'APR',
    'MAY',
    'JUN',
    'JUL',
    'AUG',
    'SEP',
    'OCT',
    'NOV',
    'DEC',
  ];
  const result = {};
  try {
    const monthCount = await RandomData
      .aggregate([
        { $match: { ActiveIndicator: 'Y', ExpiryDate: null } },
        { $project: { month: { $month: '$EffectiveDate' } } },
        { $group: { _id: '$month', count: { $sum: 1 } } },
      ]);
    months.forEach((val, index) => {
      result[val] = monthCount.find((item) => item._id === index + 1).count;
    });
    return Promise.resolve(result);
  } catch (e) {
    return Promise.reject(e);
  }
};

module.exports = {
  getMonthlyCount,
};
