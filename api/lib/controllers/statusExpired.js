/**
 * @file Update records as expired
 *
 * @author Saidul Islam Bhuiyan
 * @since 18 July, 2020
 */

const { RandomData } = require('../models/randomData');

const statusExpired = () => RandomData.update({ ActiveIndicator: 'Y', ExpiryDate: null }, { ExpiryDate: Date.now() }, { multi: true });

module.exports = {
  statusExpired,
};
