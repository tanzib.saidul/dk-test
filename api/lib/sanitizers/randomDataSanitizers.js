const Joi = require('@hapi/joi');

const getRandomDataRequest = Joi.object().keys({
  page: Joi.number().min(0).default(0),
  limit: Joi.number().min(0).default(10),
  sort: Joi.string().allow('').default('createdAt'),
  searchKey: Joi.string().allow(''),
  searchValue: Joi.string().allow(''),
});

module.exports = {
  getRandomDataRequest,
};
