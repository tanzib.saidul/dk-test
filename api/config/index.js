const config = {
  dev: {
    server: {
      port: process.env.PORT || 5000,
    },
    mongo: {
      host: process.env.MONGO_HOST || 'localhost',
      port: process.env.MONGO_PORT || 27017,
    },
  },
  test: {
    server: {
      port: process.env.PORT || 5000,
    },
    mongo: {
      host: process.env.MONGO_HOST || 'localhost',
      port: process.env.MONGO_PORT || 27017,
    },
  },
  prod: {
    server: {
      port: process.env.PORT || 5000,
    },
    mongo: {
      host: process.env.MONGO_HOST || 'localhost',
      port: process.env.MONGO_PORT || 27017,
    },
  },
};

module.exports = () => {
  if (process.env.NODE_ENV === 'production') {
    return config.prod;
  }
  if (process.env.NODE_ENV === 'test') {
    return config.test;
  }
  return config.dev;
};
