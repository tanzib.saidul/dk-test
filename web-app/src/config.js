require('dotenv').config()
const config = {
  dev: {
    domain: 'http://localhost:5000',
  },
  test: {
    domain: 'http://localhost:5000',
  },
  prod: {
    domain: 'http://68.183.185.81:5000',
  },
};

const getConfig = () => {
  if (process.env.NODE_ENV === 'production') {
    return config.prod;
  }
  if (process.env.NODE_ENV === 'test') {
    return config.test;
  }
  return config.dev;
};
export {
  getConfig
}
