import Dashboard from "views/Dashboard.js";
import Analytics from "views/Analytics.js";
import List from "views/List.js";

var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "fas fa-tachometer-alt",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/analytics",
    name: "Analytics",
    icon: "fas fa-chart-area",
    component: Analytics,
    layout: "/admin"
  },
  {
    path: "/list",
    name: "List",
    icon: "far fa-list-alt",
    component: List,
    layout: "/admin"
  },
];
export default routes;
