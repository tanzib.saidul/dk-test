import React from "react";
import { Line, Pie } from "react-chartjs-2";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col,
} from "reactstrap";

import {
  lineChartOptions,
} from "variables/charts.js";
import { getConfig } from '../config';

class Analytics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bigChartData: "data1",
      pieData : null,
      lineData: null,
      optPie: {
        legend: {
          labels: {
            fontColor: "white"
          }
        }
      }
    };
  }
  setBgChartData = name => {
    this.setState({
      bigChartData: name
    });
  };

  setPieChartData = () => {
    return fetch(`${getConfig().domain}/random/count/active`)
      .then(data => {
        return data.json();
      })
      .then(json => {
        const pieData = {
          datasets: [{
            data: [json.totalActive, json.totalInactive],
            backgroundColor: ['#36A2EB','#FF6384'],
          }],
          labels: ['Y','N']
        }
        this.setState({pieData})
      })
  }

  setLineChartData = () => {
    return fetch(`${getConfig().domain}/random/count/month`)
      .then(data => {
        return data.json();
      })
      .then(json => {
        const labels = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
        const counts = labels.map(item => json[item])
        const lineData = {
          labels,
          datasets: [
            {
              label: "My First dataset",
              fill: true,
              borderColor: "#1f8ef1",
              borderWidth: 2,
              borderDash: [],
              borderDashOffset: 0.0,
              pointBackgroundColor: "#1f8ef1",
              pointBorderColor: "rgba(255,255,255,0)",
              pointHoverBackgroundColor: "#1f8ef1",
              pointBorderWidth: 20,
              pointHoverRadius: 4,
              pointHoverBorderWidth: 15,
              pointRadius: 4,
              data: counts
            }
          ]
        }
        this.setState({lineData})
      })
  }
  async componentDidMount() {
    await this.setPieChartData()
    await this.setLineChartData()
  }

  render() {
    if(this.state.pieData === null || this.state.lineData === null) {
      return (
        <div className="content">
          <Row>
            <Col xs="12">
              <h1>...Loading</h1>
            </Col>
          </Row>
        </div>
      )
    }
    return (
      <>
        <div className="content">
          <Row>
            <Col xs="12">
              <Card className="card-chart">
                <CardHeader>
                  <Row>
                    <Col className="text-left" sm="6">
                      <CardTitle tag="h2">Active Records Effective date by months</CardTitle>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={this.state.lineData}
                      options={lineChartOptions}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col lg="12">
              <Card className="card-chart">
                <CardHeader>
                  <CardTitle tag="h2">Active and inactive records</CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="">
                    <Pie
                      data={this.state.pieData} options={this.state.optPie} height={100}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Analytics;
