
import React from "react";
import {getConfig} from '../config';
import {
  Alert,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";
import ReactLoading from 'react-loading';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      generating: false,
      alert:{
        message: '',
        isError: false,
      }
    };
  }
  actionMessage = (msg,isError= false) => {
    this.setState({
      generating:false,
      alert:{
        message: msg,
        isError,
      }
    })
    setTimeout(() => {
      this.setState({alert:{message: '',isError: false}})
    },3000)
  }

  generate = () => {
    this.setState({generating:true})
    fetch(`${getConfig().domain}/random/`,{method:'POST'})
      .then(data => {
        this.actionMessage('Successfully loaded 10,000 random data')
      })
      .catch(err => {
        this.actionMessage('Action Unsuccessful',true)
      });
  }
  reset = () => {
    this.setState({generating:true})
    fetch(`${getConfig().domain}/random/`,{method:'DELETE'})
      .then(data => {
        this.actionMessage('Reset successful')
      })
      .catch(err => {
        this.actionMessage('Action Unsuccessful',true)
      });
  }

  delete = () => {
    this.setState({generating:true})
    fetch(`${getConfig().domain}/random/status-delete`,{method:'PUT'})
      .then(data => {
        this.actionMessage('Delete Action successful')
      })
      .catch(err => {
        this.actionMessage('Action Unsuccessful',true)
      });
  }

  expiry = () => {
    fetch(`${getConfig().domain}/random/status-expired`,{method:'PUT'})
      .then(data => {
        this.actionMessage('Expiry Action successful')
      })
      .catch(err => {
        this.actionMessage('Action Unsuccessful',true)
      });
  }

  render() {
    return (
      <>
        <div className="content">
          <Row className="justify-content-md-center">
            <Col lg="6" md="12">
              <Card>
                <Col md="12" xs="12">
                  {this.state.generating?<ReactLoading className={"in-middle"} type={"bars"}/>:''}
                </Col>
                <CardHeader>
                  <CardTitle tag="h3" className={"text-center"}>Data Generation Actions</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                      <tr>
                        <th>Action</th>
                        <th className={"text-right"}>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <Button disabled={this.state.generating} color="secondary" onClick={this.generate} size="sm" type="button">
                            Generate
                          </Button>
                        </td>
                        <td className="text-right">Generate 10,000 random records</td>
                      </tr>
                      <tr>
                        <td>
                          <Button disabled={this.state.generating} onClick={this.reset} color="secondary" size="sm" type="button">
                            Reset
                          </Button>
                        </td>
                        <td className="text-right">Delete All records</td>
                      </tr>
                      <tr>
                        <td>
                          <Button disabled={this.state.generating} onClick={this.delete} color="secondary" size="sm" type="button">
                            Delete
                          </Button>
                        </td>
                        <td className="text-right">Change rows from ActiveIndicator ‘Y’ to ‘N’ and ExpiryDate to the
                          current date.</td>
                      </tr>
                      <tr>
                        <td>
                          <Button disabled={this.state.generating} onClick={this.expiry} color="secondary" size="sm" type="button">
                            Expiry
                          </Button>
                        </td>
                        <td className="text-right">Change all active rows (ExpiryDate is null) to expired rows (ExpiryDate is
                          the current date)</td>
                      </tr>
                    </tbody>
                  </Table>
                  {this.state.alert.message?(
                    <Alert color={this.state.alert.isError?'danger':'success'}>
                      {this.state.alert.message}
                    </Alert>
                  ): ''}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Dashboard;
