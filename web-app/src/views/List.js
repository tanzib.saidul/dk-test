import React from "react";
import {getConfig} from '../config';

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import Pagination from "react-js-pagination";

class List extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      records: [],
      page: 1,
      limit: 5,
      sort: 'createdAt',
      searchKey: 'FirstName',
      searchValue: '',
      totalMatched: 0
    };
  }
  getRecords =() => {
    let url = new URL(`${getConfig().domain}/random`)
    url.search = new URLSearchParams({
      page: this.state.page - 1,
      limit: this.state.limit,
      sort: this.state.sort,
      searchKey: this.state.searchKey,
      searchValue: this.state.searchValue,
    })
    return fetch(url)
      .then(data =>  data.json())
      .then(json => {
        this.setState({records:json.result.data,totalMatched:json.total_matched})
      })
  }
  onSearchValueUpdate = (e) => {
    this.setState({searchValue: e.target.value},() => {
      this.getRecords()
    })
  }
  onSearchFieldUpdate = (e) => {
    this.setState({searchKey: e.target.value},() => {
      this.getRecords()
    })
  }
  onSortFieldUpdate = (e) => {
    this.setState({sort: e.target.value},() => {
      this.getRecords()
    })
  }
  onOrderFieldUpdate = (e) => {
    let sort = this.state.sort
    if(e.target.value === 'asc'){
      sort = `-${sort}`
    } else{
      sort = sort.replace('-','')
    }
    this.setState({sort},() => {
      this.getRecords()
    })
  }
  onItemPerPageUpdate = (e) => {
    this.setState({limit: Number(e.target.value)},() => {
      this.getRecords()
    })
  }
  onPaginationUpdate = (pageNumber) => {
    this.setState({page: Number(pageNumber)},() => {
      this.getRecords()
    })
  }

  componentDidMount() {
    this.getRecords()
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="3">
              <FormGroup>
                <Label>Insert Value</Label>
                <Input placeholder="Search" type="text" onChange={this.onSearchValueUpdate}/>
              </FormGroup>
            </Col>
            <Col md="2">
              <FormGroup>
                <Label>Select Field For Search</Label>
                <Input type="select" name="select" onChange={this.onSearchFieldUpdate}>
                  <option value={"FirstName"}>First Name</option>
                  <option value={"LastName"}>Last Name</option>
                  <option value={"JobTitle"}>Job Title</option>
                  <option value={"CompanyName"}>Company Name</option>
                  <option value={"Phone"}>Phone</option>
                  <option value={"Email"}>Email</option>
                  <option value={"Country"}>Country</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md="2">
              <FormGroup>
                <Label>Sort By</Label>
                <Input type="select" name="select" onChange={this.onSortFieldUpdate}>
                  <option value={"createdAt"}>Created</option>
                  <option value={"FirstName"}>First Name</option>
                  <option value={"LastName"}>Last Name</option>
                  <option value={"Age"}>Age</option>
                  <option value={"Gender"}>Gender</option>
                  <option value={"JobTitle"}>Job Title</option>
                  <option value={"Married"}>Married</option>
                  <option value={"CompanyName"}>Company Name</option>
                  <option value={"Phone"}>Phone</option>
                  <option value={"Email"}>Email</option>
                  <option value={"Country"}>Country</option>
                  <option value={"ActiveIndicator"}>Active Indicator</option>
                  <option value={"ExpiryDate"}>Expiry Date</option>
                  <option value={"EffectiveDate"}>Effective Date</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md="2">
              <FormGroup>
                <Label>Order By</Label>
                <Input type="select" name="select" onChange={this.onOrderFieldUpdate}>
                  <option value={"desc"}>Descending</option>
                  <option value={"asc"}>Ascending</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md="2">
              <FormGroup>
                <Label>Item Per Page</Label>
                <Input type="select" name="select" onChange={this.onItemPerPageUpdate}>
                  <option value={"5"}>5</option>
                  <option value={"10"}>10</option>
                  <option value={"20"}>20</option>
                  <option value={"50"}>50</option>
                  <option value={"100"}>100</option>
                  <option value={"200"}>200</option>
                </Input>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Total Matched Items: {this.state.totalMatched}</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                    <tr>
                      <th>Created At</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Age</th>
                      <th>Gender</th>
                      <th>Job Title</th>
                      <th>Married</th>
                      <th>Company Name</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Country</th>
                      <th>Active Indicator</th>
                      <th>Expiry Date</th>
                      <th>Effective Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.records.map(record => {
                      return (
                        <tr key={record._id}>
                          <td>{new Date(record.createdAt).toDateString()}</td>
                          <td>{record.FirstName}</td>
                          <td>{record.LastName}</td>
                          <td>{record.Age}</td>
                          <td>{record.Gender}</td>
                          <td>{record.JobTitle}</td>
                          <td>{record.Married ? 'YES': 'NO'}</td>
                          <td>{record.CompanyName}</td>
                          <td>{record.Phone}</td>
                          <td>{record.Email}</td>
                          <td>{record.Country}</td>
                          <td>{record.ActiveIndicator}</td>
                          <td>{record.ExpiryDate ? record.ExpiryDate : 'N/A'}</td>
                          <td>{new Date(record.EffectiveDate).toDateString()}</td>
                        </tr>
                      )
                    })}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col md="12" className={"pagination-box"}>
              <div>
                <Pagination
                  activePage={this.state.page}
                  itemsCountPerPage={this.state.limit}
                  totalItemsCount={this.state.totalMatched}
                  pageRangeDisplayed={5}
                  onChange={this.onPaginationUpdate.bind(this)}
                />
              </div>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default List;
