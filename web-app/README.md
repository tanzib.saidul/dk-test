# [Dashboard]

This a react-bootstrap dashboard for random data manipulation and getting insights.

## Table of Contents

* [Quick Start](#quick-start)
* [File Structure](#file-structure)

## Quick Start

- You will need ```node``` and ```npm``` installed globally on your machine.
- Set the api server address in (web-app/src/config.js).config.dev
- Make sure dk-test/api backend service is running 

```
$ npm install
$ npm run start
```

## File Structure

Within the download you'll find the following directories and files:

```
dk-test
.
├── package.json
├── LICENSE.md
├── README.md
├── public
│   ├── index.html
│   └── manifest.json
└── src
    ├── config.js
    ├── index.js
    ├── logo.svg
    ├── routes.js
    ├── assets
    │   ├── css
    │   ├── demo
    │   ├── fonts
    │   ├── img
    │   └── scss
    │       ├── black-dashboard
    │       │   ├── bootstrap
    │       │   │   ├── mixins
    │       │   │   └── utilities
    │       │   ├── custom
    │       │   │   ├── cards
    │       │   │   ├── mixins
    │       │   │   ├── utilities
    │       │   │   └── vendor
    │       └── black-dashboard.scss
    ├── components
    │   ├── Navbars
    │   │   ├── AdminNavbar.js
    │   └── Sidebar
    │       └── Sidebar.js
    ├── layouts
    │   ├── Admin
    │   │   └── Admin.js
    ├── variables
    │   └── charts.js
    └── views
        ├── Analytics.js
        ├── Dashboard.js
        ├── List.js
```
